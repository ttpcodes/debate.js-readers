module.exports = function (BaseReader) {
  class AssociatedPressReader extends BaseReader {
    static get basename () {
      return 'apnews.com'
    }

    static get _homepage () {
      return 'https://apnews.com'
    }

    static get source () {
      return 'Associated Press'
    }

    _articles ($) {
      console.log($('.contentArticle').map(function () {
        return 'https://apnews.com' + this.attribs.href
      }).get())
      return $('.contentArticle').map(function () {
        return 'https://apnews.com' + this.attribs.href
      }).get()
    }

    // Yeah, the AP is really bad about author attribution.
    _authors ($) {
      let data = $('script[type="application/ld+json"]').text()
      let object = JSON.parse(data)
      let authorString = object.author.name
      let authorArray = []
      if (authorString.startsWith('By ')) {
        authorArray.push(authorString.split(' and '))
      } else {
        authorArray.push(authorString)
      }
      return authorArray
    }

    _content ($) {
      let html = ''
      let that = this
      $('.articleBody p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    _published ($) {
      // return this.$('.updatedString').text()
      let data = JSON.parse($('script[type="application/ld+json"]').text())
      return new Date(data.datePublished)
    }

    _tags ($) {
      return $('#adMetaTags').attr('content').split(',')
    }

    _title ($) {
      return $('h1').text()
    }

    /**
     * Because the Associated Press uses a JavaScript client-based homepage that
     * fetches articles dynamically, Cheerio's loading mechanism, which is
     * normally for basic DOM parsing, doesn't work. We need to use a special
     * mechanism that will allow for JavaScript to be loaded first. For this,
     * we can use JSDOM.
     */
  }
  return AssociatedPressReader
}
