const { expect } = require('chai')
const { readFile } = require('mz/fs')
const { resolve } = require('path')
const validUrl = require('valid-url')

const BaseReader = require(resolve(__dirname, '../../..', 'test/StubBaseReader'))

describe('Associated Press reader', function () {
  const AssociatedPressReader = require(resolve(__dirname, '../AssociatedPressReader'))(BaseReader)

  let normal = null

  before(async function () {
    normal = await readFile(resolve(__dirname, 'normal.txt'), 'utf8')
  })

  it('pulls articles from the homepage', async function () {
    let articles = await new AssociatedPressReader().pull()
    let areAllUris = true
    for (let article of articles) {
      if (!validUrl.isUri(article)) {
        areAllUris = false
        break
      }
    }
    expect(areAllUris).to.be.true()
  }).timeout(5000)

  it('parses a normal article', async function () {
    let article = await new AssociatedPressReader('https://www.apnews.com/3a68219dc0a1400dbaf96b263dff7251')
      .parse()
    expect(article.title).to.equal('Florida school shooting survivors are not ‘crisis actors’')
    expect(article.authors).to.deep.equal(['The Associated Press'])
    expect(article.datetime).to.deep.equal(new Date('2018-02-21 21:47:33'))
    expect(article.content).to.equal(normal)
  }).timeout(5000)
})
