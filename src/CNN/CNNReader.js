module.exports = function (BaseReader) {
  class CNNReader extends BaseReader {
    static get basename () {
      return 'www.cnn.com'
    }

    static get source () {
      return 'CNN'
    }

    get author () {
      // let authors = [];
      // let that = this;
      // this.$('.byline-dateline span[itemprop=name]').each(function() {
      //     authors.push(that.$(this).text());
      // });
      // return authors.join(", ");
      return this.$('meta[name=author]').attr('content')
    }

    get content () {
      let html = ''
      let that = this
      this.$('#body-text .l-container .zn-body__paragraph').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      console.log(this.$('meta[name=lastmod]').attr('content'))
      return this.$('meta[name=lastmod]').attr('content')
    }

    get title () {
      return this.$('h1').text()
    }
  }
  return CNNReader
}
