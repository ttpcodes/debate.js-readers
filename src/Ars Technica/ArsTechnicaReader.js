module.exports = function (BaseReader) {
  class ArsTechnicaReader extends BaseReader {
    static get basename () {
      return 'arstechnica.com'
    }

    static get _homepage () {
      return 'https://arstechnica.com'
    }

    static get source () {
      return 'Ars Technica'
    }

    _articles ($) {
      return $('article header h2 a').map(function () {
        return this.attribs.href
      }).get()
    }

    _authors ($) {
      return [$('span[itemprop=name]').text()]
    }

    _content ($) {
      let html = ''
      $('.article-content > p').each(function () {
        html += $('<div>').append($(this).clone()).html()
      })
      return html
    }

    _published ($) {
      return new Date($('time').attr('datetime'))
    }

    _tags ($) {
      let js = $('head script[type="text/javascript"]:nth-of-type(2)').html()
      let data = js.substring(js.indexOf('{'), js.lastIndexOf('}') + 1)
      return JSON.parse(data).keywords.display.split('|')
    }

    _title ($) {
      return $('h1').text()
    }
  }
  return ArsTechnicaReader
}
