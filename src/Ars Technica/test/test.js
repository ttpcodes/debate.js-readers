const chai = require('chai')
const dirtyChai = require('dirty-chai')
const { readFile } = require('mz/fs')
const { resolve } = require('path')
const validUrl = require('valid-url')

const BaseReader = require(resolve(__dirname, '../../..', 'test/StubBaseReader'))
const expect = chai.expect

chai.use(dirtyChai)

describe('Ars Technica reader', function () {
  const ArsTechnicaReader = require(resolve(__dirname, '../ArsTechnicaReader'))(BaseReader)

  let normal = null

  before(async function () {
    normal = await readFile(resolve(__dirname, 'normal.txt'), 'utf8')
  })

  it('pulls articles from the homepage', async function () {
    let articles = await new ArsTechnicaReader().pull()
    let areAllUris = true
    for (let article of articles) {
      if (!validUrl.isUri(article)) {
        areAllUris = false
        break
      }
    }
    expect(areAllUris).to.be.true()
  }).timeout(5000)

  it('parses a normal article', async function () {
    let article = await new ArsTechnicaReader('https://arstechnica.com/gadgets/2018/02/always-connected-windows-on-arm-machines-coming-this-quarter/')
      .parse()
    expect(article.title).to.equal('“Always Connected” Windows on ARM machines coming this quarter')
    expect(article.authors).to.deep.equal(['Peter Bright'])
    expect(article.tags).to.deep.equal(['arm', 'asus', 'hp', 'laptops', 'lenovo',
      'lte-2', 'microsoft-3', 'tablets', 'windows', 'type: report'])
    expect(article.published).to.deep.equal(new Date('2018-02-21T20:46:30+00:00'))
    expect(article.content + '\n').to.equal(normal)
  }).timeout(5000)
})
