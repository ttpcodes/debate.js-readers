module.exports = function (BaseReader) {
  class NewRepublicReader extends BaseReader {
    static get basename () {
      return 'newrepublic.com'
    }

    static get source () {
      return 'New Republic'
    }

    get author () {
      return this.$('.author-link').text()
    }

    get content () {
      let html = ''
      let that = this
      this.$('.article-text-grid > p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      return this.$('h5 time').attr('datetime')
    }

    get title () {
      return this.$('.article-headline span').text()
    }
  }
  return NewRepublicReader
}
