module.exports = function (BaseReader) {
  class ThinkProgressReader extends BaseReader {
    static get basename () {
      return 'thinkprogress.org'
    }

    static get source () {
      return 'Think Progress'
    }

    get author () {
      return this.$('.post__byline__author a').first().text()
    }

    get content () {
      let html = ''
      let that = this
      this.$('.post__content > p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      return this.$('time').attr('datetime')
    }

    get title () {
      return this.$('h1').text()
    }
  }
  return ThinkProgressReader
}
