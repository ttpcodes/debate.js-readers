const moment = require('moment-timezone')

module.exports = function (BaseReader) {
  class ChicagoTribuneReader extends BaseReader {
    static get basename () {
      return 'www.chicagotribune.com'
    }

    static get _homepage () {
      return ''
    }

    static get source () {
      return 'Chicago Tribune'
    }

    _author () {
      return this.$('span[itemprop=author]').text().trim()
    }

    _content () {
      let html = ''
      let that = this
      this.$('.trb_ar_page > p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    _published () {
      let cst = this.$('time[itemprop=datePublished]').attr('datetime')
      /**
       * This paper is published in Central Time, but isn't in a proper DateTime
       * format. As a result, we need to explicitly set the timezone ourselves.
       */
      return moment.tz(cst, 'YYYY-MM-DDTHH:mm:ss', 'America/Chicago').format()
    }

    _title () {
      return this.$('h1').text()
    }
  }
  return ChicagoTribuneReader
}
