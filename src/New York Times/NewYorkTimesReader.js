module.exports = function (BaseReader) {
  class NewYorkTimesReader extends BaseReader {
    static get basename () {
      return 'www.nytimes.com'
    }

    static get source () {
      return 'The New York Times'
    }

    get author () {
      let authors = []
      let that = this
      this.$('.byline-dateline span[itemprop=name]').each(function () {
        authors.push(that.$(this).text())
      })
      return authors.join(', ')
    }

    get content () {
      let html = ''
      let that = this
      this.$('.story-body > p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      return this.$('.byline-dateline time').attr('datetime')
    }

    get title () {
      return this.$('h1').text()
    }
  }
  return NewYorkTimesReader
}
